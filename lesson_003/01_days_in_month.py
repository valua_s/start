# -*- coding: utf-8 -*-

# (if/elif/else)

# По номеру месяца вывести кол-во дней в нем (без указания названия месяца, в феврале 28 дней)
# Результат проверки вывести на консоль
# Если номер месяца некорректен - сообщить об этом

# Номер месяца получать от пользователя следующим образом
month = input("Введите, пожалуйста, номер месяца: ")
month = int(month)
print('Вы ввели', month)

if month == 1:
    print('Январь')
elif month == 2:
    print('Февраль')
elif month == 3:
    print('Март')
elif month == 4:
    print('Апрель')
elif month == 5:
    print('Май')
elif month == 6:
    print('Июнь')
elif month == 7:
    print('Июль')
elif month == 8:
    print('Август')
elif month == 9:
    print('Сентябрь')
elif month == 10:
    print('Октябрь')
elif month == 11:
    print('Ноябрь')
elif month == 12:
    print('Декабрь')
else:
    print('Введите значение до 12')
